﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wilailuk.DataAccess;

namespace Wilailuk.Business
{
    public class CustomerQuery
    {

        public CustomerResult GetCustomer(CustomerPO objPO)
        {
            CustomerResult objRet = new CustomerResult();

            using (WSModel db = new WSModel())
            {

                var objRetQuery = db.Customers.AsQueryable();
                if (objPO.CustomerID != 0)
                {
                    objRetQuery = objRetQuery.Where(x => x.CustomerID == objPO.CustomerID);
                }
                if (!string.IsNullOrEmpty( objPO.Email))
                {
                    objRetQuery = objRetQuery.Where(x =>x.ContactEmail == objPO.Email);
                }
                if (objRetQuery != null && objRetQuery.Any())
                {
                    objRet = objRetQuery.AsQueryable().Include(c => c.Transactions).Select(a => new CustomerResult()
                    {
                        CustomerID = a.CustomerID,
                        CustomerName = a.CustomerName,
                        ContactEmail = a.ContactEmail,
                        MobileNo = a.MobileNo,
                        TransactionsResult = a.Transactions.OrderBy(q => q.TransactionDate).Skip((1 - 1) * 5).Take(5).Select(s => new TransactionsResult()
                        {
                            TransactionID = s.TransactionID,
                            CustomerID = s.CustomerID,
                            TransactionDate = s.TransactionDate,
                            Amount = s.Amount,
                            CurrencyCode = s.CurrencyCode,
                            Status = s.Status,
                        }).ToList()
                    }).FirstOrDefault();
                }
            }
            return objRet;

        }
    }
}
