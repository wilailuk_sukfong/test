﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wilailuk.Business
{
   public class TransactionsResult
    {
        public long TransactionID { get; set; }
       
        public long CustomerID { get; set; }
       
        public DateTime TransactionDate { get; set; }
        
        public decimal Amount { get; set; }
       
        public string CurrencyCode { get; set; }
     
        public string Status { get; set; }
    }
}
