﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wilailuk.Business
{
    public class CustomerResult
    {
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string ContactEmail { get; set; }
        public int MobileNo { get; set; }
        public List<TransactionsResult> TransactionsResult { get; set; }
    }
}
