namespace Wilailuk.DataAccess
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class WSModel : DbContext
    {
        public WSModel()
            : base("name=WSModel")
        {
        }

        public DbSet<Customers> Customers { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customers>()
          .HasMany(e => e.Transactions)
          .WithRequired(e => e.Customers)
          .HasForeignKey(e => e.CustomerID)
          .WillCascadeOnDelete(false);
        }
    }
 
}