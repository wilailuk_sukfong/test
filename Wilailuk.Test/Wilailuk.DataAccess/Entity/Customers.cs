﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wilailuk.DataAccess
{
    public partial class  Customers
    {
        public Customers()
        {
            Transactions = new HashSet<Transactions>();
        }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CustomerID { get; set; }
        [Required]
        [StringLength(30)]
        public string CustomerName { get; set; }
        [Required]
        [StringLength(25)]
        public string ContactEmail { get; set; }
        [Required]
        public int MobileNo { get; set; }
        public virtual ICollection<Transactions> Transactions { get; set; }
    }

}
 
