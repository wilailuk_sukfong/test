﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wilailuk.DataAccess
{
    public partial class Transactions
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TransactionID { get; set; }
        [Required]
        public long CustomerID { get; set; }
        [Required]
        public DateTime TransactionDate { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        [StringLength(3)]
        public string CurrencyCode { get; set; }
        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        public virtual Customers Customers { get; set; }
    }

}
 
