﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Wilailuk.Business;

namespace Wilailuk.Services.Controllers
{
    public class CustomerApiController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetCustomerByID(int customerID)
        {
            try
            {
                if(customerID==0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Customer ID");
                }
                CustomerQuery oService = new CustomerQuery();
                CustomerPO objPO = new CustomerPO() {
                     CustomerID = customerID,
                }; 

                var queryDataManager = oService.GetCustomer(objPO);
                if (queryDataManager == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No inquiry criteria");
                }
                return Request.CreateResponse(HttpStatusCode.OK, queryDataManager);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.ToString());

            }
        }
        [HttpGet]
        public HttpResponseMessage GetCustomerByEmail(string email)
        {
            try
            {
                if (!IsValidEmail(email))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Email");
                }
                CustomerQuery oService = new CustomerQuery();
                CustomerPO objPO = new CustomerPO()
                {
                    Email = email,
                };

                var queryDataManager = oService.GetCustomer(objPO);

                if (queryDataManager == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No inquiry criteria");
                }
                return Request.CreateResponse(HttpStatusCode.OK, queryDataManager);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.ToString());

            }
        }
        [HttpGet]
        public HttpResponseMessage GetCustomer(int customerID, string email)
        {
            try
            {
                CustomerQuery oService = new CustomerQuery();
                CustomerPO objPO = new CustomerPO()
                {
                    CustomerID = customerID,
                    Email  = email
                };

                var queryDataManager = oService.GetCustomer(objPO);
                if (queryDataManager == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No inquiry criteria");
                }
                return Request.CreateResponse(HttpStatusCode.OK, queryDataManager);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.ToString());

            }
        }


        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
